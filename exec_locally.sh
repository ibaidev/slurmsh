#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

EXPDIR=$(pwd)
BINDIR=$(dirname "${BASH_SOURCE[0]}")

################################################################################
# Qsub functions
################################################################################

function exec_job {
  local pymodule=($1)
  local ntrials=($2)
  local params=(${@:3})
  local jobname=${params[@]}
  jobname=${jobname// /_}
  jobname=${jobname//./_}
  jobname=${jobname////_}
  echo $jobname

  for (( trial=1; trial<=$ntrials; trial++ ))
  do
    export PYTHONUNBUFFERED=1         # Run python without buffer
    export PYTHONHASHSEED=$trial      # Set python hash seed
    export OPENBLAS_NUM_THREADS=1     # Limit thread number
    export OPENBLAS_CORETYPE=Nehalem  # Disable AVX and AV2 instructions

    source .env/bin/activate
    echo "python -m "$PYMODULE ${params[@]} $trial
    python -m $PYMODULE ${params[@]} $trial
    deactivate
  done
}

################################################################################
# Read configs
################################################################################

source ${BINDIR}/bin/config_reader.sh

################################################################################
# Read params
################################################################################

PYMODULE=$1

################################################################################
# Load Configs
################################################################################

CONFIGS=($(read_configs $EXPDIR/${PYMODULE//.//}.yml) )

NTRIALS=${CONFIGS[@]: -1}

################################################################################
# Exec qsub
################################################################################

for ((I_CONFIG=0; I_CONFIG<${#CONFIGS[@]}-1; I_CONFIG++)) do
  CONFIG="${CONFIGS[$I_CONFIG]}"
  exec_job $PYMODULE $NTRIALS ${CONFIG//;/ }
done

