# slurmsh

A set of bash scripts to run python programs on Slurm Workload Manager.

As an example, we will copy the following project and create a git repository
with it.
```bash
cp -r example_project/ ../
cd ../example_project/
git init
git config user.email 'MAIL'
git config user.name 'NAME'
git config credential.helper 'cache --timeout=300'
git config push.default simple
git add .
git commit -m "Initial commit"
```

Now, we can create the virtual environment and execute the python module
locally.
```bash
../slurmsh/build_env_locally.sh
. .env/bin/activate
python -m experiments.example of1 algorithm1 1
deactivate
cat output/result_of1_algorithm1_1.csv
```

To execute the module on the Workload Manager, first, we have to upload the
files.
```bash
../slurmsh/create_experiment.sh <user> <host> example1
```

If a YAML configuration file is placed with the same name in the same directory
of the module, it can be executed directly. It runs all the possible
combinations between the configs specified in the config file. The number for
repetitions per configuration is also set in this file, with *trials* variable.
```bash
../slurmsh/create_experiment.sh <user> <host> example1 experiments.example
```

Finally, we can return the results to our local machine.
```bash
../slurmsh/return_results.sh <user> <host>
```
