#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

PROJECTDIR=$(pwd)

PYVERSION=$1

################################################################################
# Reset packages dir
################################################################################

rm -rf $PROJECTDIR/packages
echo "mkdir $PROJECTDIR/packages"
mkdir $PROJECTDIR/packages

################################################################################
# Download packages
################################################################################
echo ">>>>>>> DOWNLOADING PACKAGES <<<<<<<<<"
if [ "$PYVERSION" = "2" ]; then
  python2 -m virtualenv $PROJECTDIR/.downloadenv
else
  python3 -m venv $PROJECTDIR/.downloadenv
fi
source $PROJECTDIR/.downloadenv/bin/activate
python -m pip install --upgrade pip
python -m pip download pip -d $PROJECTDIR/packages -f $PROJECTDIR/packages
python -m pip download -r requirements.txt -d $PROJECTDIR/packages \
  -f $PROJECTDIR/packages
deactivate
rm -rf $PROJECTDIR/.downloadenv

################################################################################
# Create new environment
################################################################################
echo ">>>>>>> CREATING ENVIRONMENT <<<<<<<<<"

rm -rf $PROJECTDIR/.env
if [ "$PYVERSION" = "2" ]; then
  python2 -m virtualenv $PROJECTDIR/.env
else
  python3 -m venv $PROJECTDIR/.env
fi
source $PROJECTDIR/.env/bin/activate
python -m pip install --upgrade pip --no-index -f $PROJECTDIR/packages
python -m pip install -r requirements.txt --no-cache-dir --no-index \
  -f $PROJECTDIR/packages
deactivate

