#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

WORKDIR=$(pwd)

################################################################################
# Create virtualenv
################################################################################
ssh pruebas << EOF # TODO This is node specific
  python3 -m virtualenv --no-download $WORKDIR/.downloadenv
EOF

source $WORKDIR/.downloadenv/bin/activate
python -m pip install --upgrade pip

################################################################################
# Execute task
################################################################################

python -m pip download pip -d $WORKDIR/packages -f $WORKDIR/packages

export LC_ALL="en_US.UTF-8" # TODO remove. Needed to deal with DEAP package
python -m pip download -r $WORKDIR/requirements.txt \
  -d $WORKDIR/packages -f $WORKDIR/packages

################################################################################
# Clean the scratch dir
################################################################################

deactivate
rm -rf $WORKDIR/.downloadenv
