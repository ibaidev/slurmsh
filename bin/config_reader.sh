#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

################################################################################
# Read configs
################################################################################

function read_configs() {
    local yaml_file=$1
    local s
    local w
    local fs

    s='[[:space:]]*'
    w='[a-zA-Z0-9_.-]*'
    fs="$(echo @|tr @ '\034')"

    (
        sed -ne '/^--/s|--||g; s|\"|\\\"|g; s/\s*$//g;' \
            -e "/#.*[\"\']/!s| #.*||g; /^#/s|#.*||g;" \
            -e  "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
            -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" |

        awk -F"$fs" 'BEGIN {
            i = -1;
            j = -1;
        }{
            indent = length($1)/2;
            vjobname[indent] = $2;
            value = $3;
            if (vjobname[0] == "configs" && indent > 0) {
                if (length(vjobname[indent]) > 0) {
                    i = i + 1;
                    j = -1;
                } else {
                    j = j + 1;
                    result[i, j] = value;
                    max[i] = j + 1;
                }
            }
            if (vjobname[0] == "trials" && indent == 0) {
                trials = value;
            }
        } END {
            ind_len = 1;
            mul = 1;
            for (j in max) {
                mul = mul * max[j];
                indices[j] = 0;
                ind_len = ind_len + 1;
            }
            for (i = 0; i < mul; i++) {
                for (j = 0; j < ind_len; j++){
                    printf("%s;", result[j, indices[j]]);
                }
                printf("\n");
                carry = 1;
                for (j = 0; j < ind_len; j++){
                    if (carry){
                        indices[j] = indices[j]+1;
                    }
                    carry = 0;
                    if (indices[j] >= max[j]){
                        carry = 1;
                        indices[j] = 0;
                    }
                }                
            }
            printf("%s", trials);
        }'

    ) < "$yaml_file"
}

