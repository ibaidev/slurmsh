#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

EXPDIR=$(pwd)
EXPNAME=$(basename $EXPDIR)
BINDIR=$(dirname "${BASH_SOURCE[0]}")

################################################################################
# Qsub functions
################################################################################

function exec_job {
  local pymodule=($1)
  local taskid=($2)
  local params=(${@:3})
  local jobname=${params[@]}
  jobname=${jobname// /_}
  jobname=${jobname//./_}
  jobname=${jobname////_}
  jobname=${EXPNAME}_$jobname
  local cmd="sbatch \
    --job-name=$jobname \
    $EXPDIR/bin/exec_task.sbatch \
    $pymodule \
    $taskid \
    ${params[@]}"
  echo $cmd
  eval $cmd
}

################################################################################
# Read configs
################################################################################

source ${BINDIR}/config_reader.sh

################################################################################
# Read params
################################################################################

PYMODULE=$1

################################################################################
# Load Configs
################################################################################

CONFIGS=($(read_configs $EXPDIR/config.yml) )

NTRIALS=${CONFIGS[@]: -1}

################################################################################
# Download packages
################################################################################

bin/package_download.sh

################################################################################
# Exec qsub
################################################################################
for TASKID in $(seq 1 $NTRIALS); do
  for ((I_CONFIG=0; I_CONFIG<${#CONFIGS[@]}-1; I_CONFIG++)) do
    CONFIG="${CONFIGS[$I_CONFIG]}"
    exec_job $PYMODULE $TASKID ${CONFIG//;/ }
  done
done

