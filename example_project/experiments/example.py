# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

import sys
import csv
import collections

import numpy as np


def main(args=None):
    """
    Main function

    :param args:
    :type args:
    :return:
    :rtype:
    """

    if args is None:
        args = sys.argv[1:]

    function_name,\
        algorithm_name,\
        trial = args
    trial = int(trial)

    # Set seed
    seed = trial
    np.random.seed(seed)

    output = "output/result_{}_{}_{}".format(
        function_name, algorithm_name, trial
    )

    results = np.random.randn(5)

    print_dict(
        output,
        [
            collections.OrderedDict([
                ('function_name', function_name),
                ('algorithm_name', algorithm_name),
                ('trial', trial),
                ('eval', eval_i),
                ('result', result)
            ])
            for eval_i, result in enumerate(results)
        ],
        with_header=True
    )


def print_dict(file_name, out_dict, with_header=True):
    """

    :param file_name:
    :type file_name:
    :param out_dict:
    :type out_dict:
    :param with_header:
    :type with_header:
    :return:
    :rtype:
    """
    with open(file_name+".csv", 'w+t') as outputfile:
        dict_writter = csv.DictWriter(outputfile, out_dict[0].keys(),
                                      delimiter=',')
        if with_header:
            dict_writter.writeheader()
        dict_writter.writerows(out_dict)


if __name__ == "__main__":
    main()
