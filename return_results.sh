#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

################################################################################
# Read arguments
################################################################################

REMUSER=$1
REMHOST=$2

################################################################################
# Set directories
################################################################################

PROJECTDIR=$(pwd)
REMOTEWORKSPACE=$(realpath --relative-to=/home/$USER $PROJECTDIR)

################################################################################
# Loop for all experiments
################################################################################

mapfile -t EXPNAMES < $PROJECTDIR/experiments.txt

for ((I_EXPNAME=0; I_EXPNAME<${#EXPNAMES[@]}; I_EXPNAME++)) do
  EXPNAME="${EXPNAMES[$I_EXPNAME]}"
  echo $EXPNAME
  REMOTEPDIR=${REMOTEWORKSPACE}/$EXPNAME

  ssh $REMUSER@$REMHOST "ls $REMOTEPDIR/output/result_*.csv | head -1 | " \
    "xargs head -1 > $REMOTEPDIR/results/results.csv"

  ssh $REMUSER@$REMHOST "awk 'FNR>1' $REMOTEPDIR/output/result_*.csv >> " \
    "$REMOTEPDIR/results/results.csv;"

  rsync -q -av $REMUSER@$REMHOST:$REMOTEPDIR/results/results.csv \
    $PROJECTDIR/results/${EXPNAME}_results.csv
done

