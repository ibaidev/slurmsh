#!/usr/bin/env bash
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Ibai Roman
#
#   This file is part of slurmsh.
#
#   slurmsh is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   slurmsh is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with slurmsh. If not, see <http://www.gnu.org/licenses/>.

################################################################################
# Read arguments
################################################################################

REMUSER=$1
REMHOST=$2

if [ -z "$3" ]; then
  EXPNAME=$(date "+%Y_%m_%d_%H_%M_%S")
else
  EXPNAME=$3
fi

if [ "$4" ]; then
  PYMODULE=$4
fi

################################################################################
# Set directories
################################################################################

PROJECTDIR=$(pwd)
SLURMSHDIR=$PROJECTDIR/../slurmsh
REMOTEEXPDIR=$(realpath --relative-to=/home/$USER $PROJECTDIR)
REMOTEEXPDIR=${REMOTEEXPDIR}/$EXPNAME

echo $EXPNAME >> $PROJECTDIR/experiments.txt

################################################################################
# Pack files
################################################################################

git archive --format=tar.gz -o $PROJECTDIR/source.tar.gz HEAD

################################################################################
# Upload files
################################################################################

ssh $REMUSER@$REMHOST "mkdir -p $REMOTEEXPDIR"
ssh $REMUSER@$REMHOST "mkdir $REMOTEEXPDIR/output"
ssh $REMUSER@$REMHOST "mkdir $REMOTEEXPDIR/log"
ssh $REMUSER@$REMHOST "mkdir $REMOTEEXPDIR/results"
ssh $REMUSER@$REMHOST "mkdir $REMOTEEXPDIR/packages"
ssh $REMUSER@$REMHOST "mkdir $REMOTEEXPDIR/bin"

rsync -q -av $SLURMSHDIR/bin/* $REMUSER@$REMHOST:$REMOTEEXPDIR/bin
rsync -q -av $PROJECTDIR/requirements.txt \
  $REMUSER@$REMHOST:$REMOTEEXPDIR/requirements.txt
rsync -q -av $PROJECTDIR/source.tar.gz \
  $REMUSER@$REMHOST:$REMOTEEXPDIR/source.tar.gz
if [ "$PYMODULE" ]; then
  rsync -q -av $PROJECTDIR/${PYMODULE//.//}.yml \
    $REMUSER@$REMHOST:$REMOTEEXPDIR/config.yml
fi

rm source.tar.gz

################################################################################
# Launch command
################################################################################

if [ "$PYMODULE" ]; then

ssh $REMUSER@$REMHOST << EOF
  cd $REMOTEEXPDIR
  bin/launch_experiment.sh $PYMODULE
EOF

fi

